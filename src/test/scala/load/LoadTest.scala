package load

import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef.http
import load.Requests._

import scala.concurrent.duration._
import scala.language.postfixOps


class LoadTest extends Simulation {

  val url1 = http.baseURL("http://10.20.10.80:8080")
  val url2 = http.baseURL("http://10.20.10.80:8080")
  val scn1 = scenario("Test1").exec(profile1)
  val scn2 = scenario("Test2").exec(profile2)

  //setUp(scn.inject(constantUsersPerSec(10) during(2 seconds)).protocols(httpConf))
  //  setUp(scn.inject(constantUsersPerSec(100) during(2 minutes))).protocols(httpConf).throttle(
  //    reachRps(200) in (10 seconds),
  //    holdFor(2 minutes),
  //    jumpToRps(50),
  //    holdFor(5 minutes)
  //  )
  val prof1 = scn1.inject(rampUsers(10000) over (5 minutes)).protocols(url1)
  val prof2 = scn2.inject(rampUsers(10000) over (5 minutes)).protocols(url2)

  setUp(prof1,prof2)

}
