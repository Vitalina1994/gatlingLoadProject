package load

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.core.feeder.RecordSeqFeederBuilder
import io.gatling.http.request.builder.HttpRequestBuilder
import io.gatling.jdbc.Predef.jdbcFeeder

import scala.language.postfixOps

object Requests {
  val profile1: HttpRequestBuilder = http("profile1")
    .get("/api/v1/inputRecord?apiKey=2f3a5355-688e-4661-92c2-7304addd767c&profile=1730c&mail=test@gmail.com")
    val profile2: HttpRequestBuilder = http("profile2")
      .get("/api/v1/inputRecord?apiKey=2f3a5355-688e-4661-92c2-7304addd767c&profile=c9d88&mail=test@gmail.com")

   // .check(status.is(200))

}
