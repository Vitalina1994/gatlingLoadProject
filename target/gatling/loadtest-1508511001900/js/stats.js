var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "369",
        "ok": "369",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38252",
        "ok": "38252",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9712",
        "ok": "9712",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6917",
        "ok": "6917",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8248",
        "ok": "8250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15482",
        "ok": "15482",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19414",
        "ok": "19414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31554",
        "ok": "31554",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 57,
        "percentage": 1
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 29,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3914,
        "percentage": 98
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "369",
        "ok": "369",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38252",
        "ok": "38252",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9712",
        "ok": "9712",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6917",
        "ok": "6917",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8248",
        "ok": "8246",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15482",
        "ok": "15482",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19414",
        "ok": "19414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31554",
        "ok": "31554",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 57,
        "percentage": 1
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 29,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3914,
        "percentage": 98
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
