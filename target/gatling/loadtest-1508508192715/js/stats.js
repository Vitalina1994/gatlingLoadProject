var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "100000",
        "ok": "86130",
        "ko": "13870"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60205",
        "ok": "60036",
        "ko": "60205"
    },
    "meanResponseTime": {
        "total": "29163",
        "ok": "24193",
        "ko": "60021"
    },
    "standardDeviation": {
        "total": "19051",
        "ok": "15599",
        "ko": "35"
    },
    "percentiles1": {
        "total": "27510",
        "ok": "23045",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "43211",
        "ok": "36625",
        "ko": "60016"
    },
    "percentiles3": {
        "total": "60011",
        "ok": "52190",
        "ko": "60101"
    },
    "percentiles4": {
        "total": "60063",
        "ok": "57814",
        "ko": "60187"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1289,
        "percentage": 1
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1144,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 83697,
        "percentage": 84
    },
    "group4": {
        "name": "failed",
        "count": 13870,
        "percentage": 14
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1351.351",
        "ok": "1163.919",
        "ko": "187.432"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "100000",
        "ok": "86130",
        "ko": "13870"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60205",
        "ok": "60036",
        "ko": "60205"
    },
    "meanResponseTime": {
        "total": "29163",
        "ok": "24193",
        "ko": "60021"
    },
    "standardDeviation": {
        "total": "19051",
        "ok": "15599",
        "ko": "35"
    },
    "percentiles1": {
        "total": "27511",
        "ok": "23046",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "43222",
        "ok": "36540",
        "ko": "60016"
    },
    "percentiles3": {
        "total": "60011",
        "ok": "52196",
        "ko": "60101"
    },
    "percentiles4": {
        "total": "60063",
        "ok": "57815",
        "ko": "60187"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 1289,
        "percentage": 1
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 1144,
        "percentage": 1
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 83697,
        "percentage": 84
    },
    "group4": {
        "name": "failed",
        "count": 13870,
        "percentage": 14
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1351.351",
        "ok": "1163.919",
        "ko": "187.432"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
