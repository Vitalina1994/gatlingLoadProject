var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "2000",
        "ok": "2000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4589",
        "ok": "4589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1523",
        "ok": "1523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1028",
        "ok": "1028",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1379",
        "ok": "1379",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3143",
        "ok": "3143",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4498",
        "ok": "4498",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 644,
        "percentage": 32
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 73,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 1283,
        "percentage": 64
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "333.333",
        "ok": "333.333",
        "ko": "-"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "2000",
        "ok": "2000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4589",
        "ok": "4589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1523",
        "ok": "1523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1028",
        "ok": "1028",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1379",
        "ok": "1379",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3143",
        "ok": "3143",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4498",
        "ok": "4498",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 644,
        "percentage": 32
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 73,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 1283,
        "percentage": 64
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "333.333",
        "ok": "333.333",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
