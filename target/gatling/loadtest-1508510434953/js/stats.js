var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4000",
        "ok": "0",
        "ko": "4000"
    },
    "minResponseTime": {
        "total": "132",
        "ok": "-",
        "ko": "132"
    },
    "maxResponseTime": {
        "total": "2539",
        "ok": "-",
        "ko": "2539"
    },
    "meanResponseTime": {
        "total": "1567",
        "ok": "-",
        "ko": "1567"
    },
    "standardDeviation": {
        "total": "402",
        "ok": "-",
        "ko": "402"
    },
    "percentiles1": {
        "total": "1702",
        "ok": "-",
        "ko": "1702"
    },
    "percentiles2": {
        "total": "1814",
        "ok": "-",
        "ko": "1814"
    },
    "percentiles3": {
        "total": "2073",
        "ok": "-",
        "ko": "2073"
    },
    "percentiles4": {
        "total": "2246",
        "ok": "-",
        "ko": "2246"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 4000,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1000",
        "ok": "-",
        "ko": "1000"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "4000",
        "ok": "0",
        "ko": "4000"
    },
    "minResponseTime": {
        "total": "132",
        "ok": "-",
        "ko": "132"
    },
    "maxResponseTime": {
        "total": "2539",
        "ok": "-",
        "ko": "2539"
    },
    "meanResponseTime": {
        "total": "1567",
        "ok": "-",
        "ko": "1567"
    },
    "standardDeviation": {
        "total": "402",
        "ok": "-",
        "ko": "402"
    },
    "percentiles1": {
        "total": "1702",
        "ok": "-",
        "ko": "1702"
    },
    "percentiles2": {
        "total": "1814",
        "ok": "-",
        "ko": "1814"
    },
    "percentiles3": {
        "total": "2073",
        "ok": "-",
        "ko": "2073"
    },
    "percentiles4": {
        "total": "2246",
        "ok": "-",
        "ko": "2246"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 4000,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "1000",
        "ok": "-",
        "ko": "1000"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
