var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9058",
        "ok": "9058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2909",
        "ok": "2909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2569",
        "ok": "2569",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5700",
        "ok": "5700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7965",
        "ok": "7965",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 191,
        "percentage": 5
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 154,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3655,
        "percentage": 91
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "363.636",
        "ok": "363.636",
        "ko": "-"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9058",
        "ok": "9058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2909",
        "ok": "2909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2569",
        "ok": "2569",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5700",
        "ok": "5700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7965",
        "ok": "7965",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 191,
        "percentage": 5
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 154,
        "percentage": 4
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3655,
        "percentage": 91
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "363.636",
        "ok": "363.636",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
