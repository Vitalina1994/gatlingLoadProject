var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "50000",
        "ok": "0",
        "ko": "50000"
    },
    "minResponseTime": {
        "total": "136",
        "ok": "-",
        "ko": "136"
    },
    "maxResponseTime": {
        "total": "55302",
        "ok": "-",
        "ko": "55302"
    },
    "meanResponseTime": {
        "total": "22208",
        "ok": "-",
        "ko": "22208"
    },
    "standardDeviation": {
        "total": "16950",
        "ok": "-",
        "ko": "16950"
    },
    "percentiles1": {
        "total": "14307",
        "ok": "-",
        "ko": "14309"
    },
    "percentiles2": {
        "total": "37090",
        "ok": "-",
        "ko": "37091"
    },
    "percentiles3": {
        "total": "53247",
        "ok": "-",
        "ko": "53247"
    },
    "percentiles4": {
        "total": "54583",
        "ok": "-",
        "ko": "54583"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 50000,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "724.638",
        "ok": "-",
        "ko": "724.638"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "50000",
        "ok": "0",
        "ko": "50000"
    },
    "minResponseTime": {
        "total": "136",
        "ok": "-",
        "ko": "136"
    },
    "maxResponseTime": {
        "total": "55302",
        "ok": "-",
        "ko": "55302"
    },
    "meanResponseTime": {
        "total": "22208",
        "ok": "-",
        "ko": "22208"
    },
    "standardDeviation": {
        "total": "16950",
        "ok": "-",
        "ko": "16950"
    },
    "percentiles1": {
        "total": "14304",
        "ok": "-",
        "ko": "14309"
    },
    "percentiles2": {
        "total": "37089",
        "ok": "-",
        "ko": "37089"
    },
    "percentiles3": {
        "total": "53247",
        "ok": "-",
        "ko": "53247"
    },
    "percentiles4": {
        "total": "54583",
        "ok": "-",
        "ko": "54583"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 50000,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "724.638",
        "ok": "-",
        "ko": "724.638"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
