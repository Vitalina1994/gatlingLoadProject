var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8000",
        "ok": "8000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3252",
        "ok": "3252",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4202",
        "ok": "4202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4968",
        "ok": "4968",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5551",
        "ok": "5551",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 497,
        "percentage": 6
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 250,
        "percentage": 3
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 7253,
        "percentage": 91
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "800",
        "ok": "800",
        "ko": "-"
    }
},
contents: {
"req_newslist-324c1": {
        type: "REQUEST",
        name: "NewsList",
path: "NewsList",
pathFormatted: "req_newslist-324c1",
stats: {
    "name": "NewsList",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "311",
        "ok": "311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4851",
        "ok": "4851",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "875",
        "ok": "875",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2173",
        "ok": "2173",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2646",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3290",
        "ok": "3290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4181",
        "ok": "4181",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 497,
        "percentage": 12
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 250,
        "percentage": 6
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 3253,
        "percentage": 81
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "400",
        "ok": "400",
        "ko": "-"
    }
}
    },"req_newslist-redire-5c645": {
        type: "REQUEST",
        name: "NewsList Redirect 1",
path: "NewsList Redirect 1",
pathFormatted: "req_newslist-redire-5c645",
stats: {
    "name": "NewsList Redirect 1",
    "numberOfRequests": {
        "total": "4000",
        "ok": "4000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2036",
        "ok": "2036",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4210",
        "ok": "4210",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "623",
        "ok": "623",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4193",
        "ok": "4193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4594",
        "ok": "4594",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5273",
        "ok": "5273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5764",
        "ok": "5764",
        "ko": "-"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 4000,
        "percentage": 100
    },
    "group4": {
        "name": "failed",
        "count": 0,
        "percentage": 0
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "400",
        "ok": "400",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
